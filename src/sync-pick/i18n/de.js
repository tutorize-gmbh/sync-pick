export default {
    searchPlaceholder: 'Suchen',
    emptySelectButtonText: 'Alle anzeigen',
    noResultsText: 'Keine Ergebnisse gefunden',
    selectedText: '%num% Einträge ausgewählt',
    selectAllButtonText: 'Alles auswählen',
    deselectAllButtonText: 'Nichts auswählen'
}
