export default {
    searchPlaceholder: 'Buscar',
    emptySelectButtonText: 'Mostrar todos',
    noResultsText: 'No se encontraron resultados',
    selectedText: '%num% seleccionados',
    selectAllButtonText: 'Seleccionar todo',
    deselectAllButtonText: 'Deseleccionar todo'
}
