export default {
    searchPlaceholder: 'Rechercher',
    emptySelectButtonText: 'afficher Tout',
    noResultsText: 'Aucun résultat trouvé',
    selectedText: '%num% sélectionnés',
    selectAllButtonText: 'sélectionner Tout',
    deselectAllButtonText: 'désélectionner Tout'
}
