export default {
    searchPlaceholder: 'Search',
    emptySelectButtonText: 'Show all',
    noResultsText: 'No results found',
    selectedText: '%num% selected',
    selectAllButtonText: 'Select all',
    deselectAllButtonText: 'Deselect all'
}
